const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const http = require('http');
const server = http.createServer(app);
const io = require('socket.io').listen(server);
app.io = io;
const dotenv = require('dotenv');
const publicIp = require('public-ip');
const updateDeviceData = require('./helpers/updateDeviceData');

// Models
const Dvmsdetail = require('./models/Dvmsdetail');
const Devices = require('./models/Devices');
const Notification = require('./models/Notification');

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.set("io", io);
app.use(morgan('tiny'));
dotenv.config();

// Functions to get data
const getData = require('./config/getData');
const getPropertyCode = require('./helpers/getPropertyCode');

// Function to refresh data in DB every 2 minutes
const updateData = require('./config/updateData');

// DB Connection
mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => { console.log('Connected to the DB') })
    .catch(err => console.log(err));

// Function to fetch data from main server and update local database
updateData();

// Fetch data from main server
const getDataFromServer = () => {
    getData.DeviceStatus();
    getData.DockerData();
    getData.SendVendorData();
    getData.AddressData();
    getData.PropertyData();
    getData.UserData();
    getData.DealerData();
    getData.DeviceData();
    getData.AgentData();
    getData.SystemTypeData();
    getData.SensorData();
    getData.MasterVendorData();
    getData.ApprovedVendorData();
}

// Get data from main server if connected to internet
function checkInternet(cb) {
    require('dns').lookup('google.com', function (err) {
        if (err && err.code == "ENOTFOUND") {
            cb(false);
        } else {
            cb(true);
        }
    })
}

checkInternet(function (isConnected) {
    if (isConnected) {
        // connected to the internet
        console.log('Connected to the internet');
        getPropertyCode()
            .then(result => {
                if (result !== null) {
                    getDataFromServer();
                } else {
                    throw new Error('Please insert property details to proceed.');
                }
            })
            .catch(err => console.log('Unable to fetch data from main server'))
    } else {
        // not connected to the internet
        console.log('Not connected to the internet. Preserving local data');
    }
});

//Routes

const notificationRoutes = require('./routes/notification');
app.use('/api/notification', notificationRoutes);

const dealerRoutes = require("./routes/dealer");
app.use("/api/dealers", dealerRoutes);

const devicesRoutes = require('./routes/device');
app.use("/api/devices", devicesRoutes);

const propertyDetailsRoutes = require('./routes/propertydetail');
app.use("/api/property", propertyDetailsRoutes);

const homeRoutes = require('./routes/homepage');
app.use("/api/home", homeRoutes);

const sensorRoutes = require('./routes/sensor');
app.use("/api/sensors", sensorRoutes);

const masterVendorRoutes = require('./routes/mastervendor');
app.use("/api/mastervendor", masterVendorRoutes);

const settingRoutes = require('./routes/setting');
app.use('/api/settings',settingRoutes);

// // Socket connection
io.on("connection", async socket => {
    //vendor events
    socket.on("dashboard", async (data) => {
        device['notification_type'] = 2
        let notification = new Notifications(data);
        let notification_timestamp = Math.round((new Date()).getTime() / 1000);
        device['notification_timestamp'] = notification_timestamp
        notification.save(function (err, result) {
            if (err) {
                return console.error(err);
            }
            console.log(result.id + "dashboard status changed - saved to notifications collection.");
            io.emit("refresh");
        });
    })
});


async function updateDashboardStatus(mac, docker_id) {
    await Devices.updateOne({ hw_address: mac, docker_id: docker_id, monitor: 1 }, { $set: { "dashboard_status": "snooze" } })
}


async function createNewNotification(device) {
    console.log('NOTIFICATION DEVICE',device);
    let notification = await new Notification(device);
    notification.save();
    io.emit("refresh");
}

//Note: notification type =>  1 - online-offline , 2 - dashboard status
//URL for real time notification 
app.get('/notification', async (req, res) => {

    let docker_id = req.query.docker_id;
    let mac = req.query.mac;
    res.json();
    let device = await Devices.findOne({ hw_address: mac, docker_id: docker_id, monitor: 1 });
    console.log('Device', device);
    if (device !== null) {
        console.log('Device is not null');
        if (device.alarm === "9") {
            console.log('Device is offline');
            //Check if device has a parent device assigned to it
            if (device.parent_device_id !== null) {
                console.log('Device has a parent device');
                let parent = await Devices.findOne({ id: device.parent_device_id });
                // Check if parent device is online
                if (parent.status === 1) {
                    //update dashboard_status of offline device to snooze and an emit offline event
                    updateDashboardStatus(mac, docker_id)
                    io.emit("offline", device);
                    //assign a notification_type, dashboard_status, current timestamp to the new notification
                    device['notification_type'] = 1;
                    device['dashboard_status'] = 'snooze';
                    let notification_timestamp = Math.round((new Date()).getTime() / 1000);
                    device['notification_timestamp'] = notification_timestamp;
                    //insert into notifications collection
                    createNewNotification(device);
                }
            } else {
                console.log('Device does not have a parent');
                //update dashboard_status of offline device to snooze and an emit offline event
                updateDashboardStatus(mac, docker_id)
                io.emit("offline", device);
                //assign a notification_type, dashboard_status, current timestamp to the new notification
                device['notification_type'] = 1
                device['dashboard_status'] = 'snooze';
                let notification_timestamp = Math.round((new Date()).getTime() / 1000);
                device['notification_timestamp'] = notification_timestamp;
                //insert into notifications collection
                createNewNotification(device)
            }
        } else if (device.alarm === "7" || device.alarm === "8") {   //check if device is online
            //clear dashboard_status & ticket when a device comes online
            await Devices.updateOne({ id: device.id }, {
                $set: {
                    "dashboard_status": null,
                    "ticket": null
                }
            }, (err, res) => {
                if (res) {
                    io.emit("online", device);
                    //assign a notification_type, dashboard_status, current timestamp to the new notification
                    device['notification_type'] = 1
                    device['dashboard_status'] = null
                    device['ticket'] = null
                    let notification_timestamp = Math.round((new Date()).getTime() / 1000);
                    device['notification_timestamp'] = notification_timestamp
                    //insert into notifications collection
                    createNewNotification(device)
                }
                else {
                    console.error(err);
                }
            });
        }
    }
});


// Start server
server.listen(process.env.PORT, async () => {

    // Find public IP and insert into dvmsdetails collection
    let publicip = await publicIp.v4();
    await Dvmsdetail.updateOne({}, { $set: { publicIp: publicip } });

    console.log(`Server started at port ${process.env.PORT}`);
});