const axios = require('axios');

// Models
const Agent = require('../models/Agent');
const Address = require('../models/Address');
const Property = require('../models/Property');
const User = require('../models/User');
const Dealer = require('../models/Dealer');
const Devices = require('../models/Devices');
const SystemType = require('../models/SystemType');
const Docker = require('../models/Docker');
const ApprovedVendor = require('../models/ApprovedVendor');
const Sensor = require('../models/Sensor');


// Helper functions 
const getPropertyCode = require('../helpers/getPropertyCode');
const getAgentId = require('../helpers/getAgentId');


async function DeviceStatus() {
    let result = await Devices.find({}, { ip_addresses: 1, agent_id: 1, docker_id: 1, hw_address: 1, status: 1, last_seen_on:1, display_name:1, vendor:1 });
    axios.put(`${process.env.link}/devices/insert`, result)
        .then(res => {})
        .catch(err => console.log(err))
}

async function DockerData() {
    let result = await Docker.find({});
    axios.put(`${process.env.link}/dockers/upsert`, result)
        .then(res => {})
        .catch(err => console.log(err))
}

async function SendVendorData(){
    let result = await ApprovedVendor.find({});
    axios.post(`${process.env.link}/docker/add`,result)
        .then(res=>{})
        .catch(err=>console.log(err));
}

const AgentData = () => {
    getAgentId()
        .then(res => {
            axios.get(`${process.env.link}/agents/${res.dvms_id}`)
                .then(res => saveData(res.data))
                .catch(err => console.log(err))

            const saveData = async (data) => {
                try {
                    await Agent.findOneAndUpdate({ id: data.id }, data, { upsert: true });
                } catch (e) {
                    console.log(e);
                }
            }
        })
        .catch(err => console.log(err))
}

// Not needed
const AddressData = () => {
    getPropertyCode()
        .then(result => {
            axios.get(`${process.env.link}/${result.property_code}/property/address`)
                .then(data => saveData(data.data))
                .catch(err => console.log(err))

            const saveData = async (data) => {
                try {
                    await Address.findOneAndUpdate({ id: data.id }, data, { upsert: true });
                } catch (e) {
                    console.log(e);
                }
            }
        })
        .catch(err => console.log(err))
}

const PropertyData = () => {
    getPropertyCode()
        .then(result => {
            axios.get(`${process.env.link}/${result.property_code}/property`)
                .then(data => saveData(data.data))
                .catch(err => console.log(err))

            const saveData = async (data) => {
                try {
                    await Property.findOneAndUpdate({ property_code: data.property_code }, data, { upsert: true });
                } catch (e) {
                    console.log(e);
                }
            }
        })
        .catch(err => console.log(err))
}

const UserData = () => {
    getPropertyCode()
        .then(result => {
            axios.get(`${process.env.link}/${result.property_code}/property/user`)
                .then(data => saveData(data.data))
                .catch(err => console.log(err))

            const saveData = async (data) => {
                try {
                    await User.findOneAndUpdate({ username: data.username }, data, { upsert: true });
                } catch (e) {
                    console.log(e);
                }
            }
        })
        .catch(err => console.log(err))
}


const DealerData = () => {
    getPropertyCode()
        .then(result => {
            axios.get(`${process.env.link}/${result.property_code}/property/dealer`)
                .then(data => saveData(data.data))
                .catch(err => console.log(err))

            const saveData = async (data) => {
                let dealer = data.map(async (dealer) => {
                    try {
                        await Dealer.findOneAndUpdate({ account_number: dealer.account_number }, dealer, { upsert: true });
                    } catch (e) {
                        console.log(e);
                        res.json(e);
                    }
                })
            }
        })
        .catch(err => console.log(err))
}


const DeviceData = () => {
    getAgentId()
        .then(result => {
            axios.get(`${process.env.link}/${result.dvms_id}/agent/device`)
                .then(data => saveData(data.data))
                .catch(err => console.log(err))

            const saveData = async (data) => {
                let device = data.map(async (device) => {
                    try {
                        await Devices.findOneAndUpdate({ hw_address: device.hw_address, docker_id: device.docker_id, agent_id: device.agent_id }, device, { upsert: true });
                    } catch (e) {
                        console.log(e);
                    }
                })
            }
        })
        .catch(err => console.log(err))
}

const SystemTypeData = () => {
    getAgentId()
        .then(res => {
            axios.get(`${process.env.link}/docker/${res.dvms_id}/agent`)
                .then(res => saveData(res.data))
                .catch(err => console.log(err));

            const saveData = async (data) => {

                data.map(async systemtype => {
                    await SystemType.findOneAndUpdate({ id: systemtype.id }, systemtype, { upsert: true });
                })
            }
        })
        .catch(err => console.log(err));
}

const SensorData = () => {
    getAgentId()
        .then(res => {
            axios.get(`${process.env.link}/agent/${res.dvms_id}/sensors`)
                .then(res => saveData(res.data))
                .catch(err => console.log(err))

            const saveData = async (sensors) => {
                sensors.map(async sensor => {
                    await Sensor.findOneAndUpdate({ sensor_id: sensor.sensor_id, device_id: sensor.device_id }, sensor, { upsert: true });
                })
            }
        })
        .catch(err => console.log(err));
}

const MasterVendorData = () => {
    getAgentId()
        .then(res => {
            axios.get(`${process.env.link}/${res.dvms_id}/mastervendors`)
                .then(res => saveData(res.data))
                .catch(err => console.log(err));

            const saveData = async (mastervendors) => {
                mastervendors.map(async mastervendor => {
                    await User.findOneAndUpdate({ username: mastervendor.username }, mastervendor, { upsert: true });
                });
            }
        })
        .catch(err => console.log(err))
}

const ApprovedVendorData = ()=>{
    getAgentId()
        .then(res=>{
            axios.get(`${process.env.link}/agent/${res.dvms_id}/activated/dockers`)
                .then(res=> saveData(res.data))
                .catch(err=>console.log(err))

            const saveData = async(vendors)=>{
                console.log('vendors');
                vendors.map(async vendor=>{
                    await ApprovedVendor.findOneAndUpdate({agent_id:vendor.agent_id},vendor,{upsert:true})
                })
            }
        })
}

module.exports = {
    AgentData: AgentData,
    AddressData: AddressData,
    PropertyData: PropertyData,
    UserData: UserData,
    DealerData: DealerData,
    DeviceData: DeviceData,
    SystemTypeData: SystemTypeData,
    SensorData: SensorData,
    MasterVendorData: MasterVendorData,
    DeviceStatus: DeviceStatus,
    DockerData: DockerData,
    SendVendorData:SendVendorData,
    ApprovedVendorData:ApprovedVendorData
}