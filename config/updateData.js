const Devices = require('../models/Devices');
const Docker = require('../models/Docker');
const axios = require('axios');
const getData = require('./getData');
const getPropertyCode = require('../helpers/getPropertyCode');
const updateDeviceData = require('../helpers/updateDeviceData');

const getDataFromServer = () => {
    getData.AddressData();
    getData.PropertyData();
    getData.UserData();
    getData.DealerData();
    getData.DeviceData();
    getData.AgentData();
    getData.SystemTypeData();
    getData.SensorData();
    getData.MasterVendorData();
    // getData.SendVendorData();
    getData.ApprovedVendorData();
}

async function DeviceStatus() {
    let result = await Devices.find({}, { ip_addresses: 1, agent_id: 1, docker_id: 1, hw_address: 1, status: 1, last_seen_on:1, display_name:1, vendor:1 })
    axios.put(`${process.env.link}/devices/insert`, result)
        .then(res => { })
        .catch(err => console.log(err))
}

async function DockerData() {
    let result = await Docker.find({});
    axios.put(`${process.env.link}/dockers/upsert`, result)
        .then(res => { })
        .catch(err => console.log(err))
}

const updateData = () => {
    setInterval(() => {
        function checkInternet(cb) {
            require('dns').lookup('google.com', function (err) {
                if (err && err.code == "ENOTFOUND") {
                    cb(false);
                } else {
                    cb(true);
                }
            })
        }

        checkInternet(function (isConnected) {
            if (isConnected) {
                getPropertyCode()
                    .then(result => {
                        if (result !== null) {
                            DeviceStatus();
                            DockerData();
                            getDataFromServer();
                            updateDeviceData();
                            console.log('Fetched latest data from main server');
                        } else {
                            throw new Error('Please insert property details to proceed.');
                        }
                    })
                    .catch(err => console.log("Unable to fetch data from main server"))
            } else {
                console.log('Not connected to the internet. Preserving local data');
            }
        });
    }, 1000 * 30)
}

module.exports = updateData;
