const express = require('express');
const router = express.Router();

// Models
const Sensor = require('../models/Sensor');
const Devices = require('../models/Devices');

// Sensors for systemtype bacnet devices
router.get('/:system_type', async (req, res) => {
    try {
        Devices.aggregate([
            {"$lookup":{"from":"docker","localField":"docker_id","foreignField":"id","as":"devices"}},
            {"$lookup":{"from":"sensors","localField":"id","foreignField":"device_id","as":"sensors"}},
            {"$match":{"sensors":{"$ne":[]}}},
            {"$project":{"_id":0,"sensors":"$sensors"}},
            {"$unwind":"$sensors"},
            ],(err, result) => {
            let newSensorList = result.map(result => { return result.sensors });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Sensors for devices in a building
router.get('/building/:building', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        Devices.aggregate([
            { "$match": { "building": building } },
            {
                "$lookup": {
                    "from": "sensors",
                    "localField": "id",
                    "foreignField": "device_id",
                    "as": "sensors"
                }
            },
            {
                "$unwind": "$sensors"
            }
        ], (err, result) => {
            let newSensorList = result.map(result => { return result.sensors });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Sensors for devices in a floor
router.get('/floor/:floor', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        Devices.aggregate([
            { "$match": { "floor": floor } },
            {
                "$lookup": {
                    "from": "sensors",
                    "localField": "id",
                    "foreignField": "device_id",
                    "as": "sensorArray"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "sensor": "$sensorArray"
                }
            },
            {
                "$unwind": "$sensor"
            }
        ], (err, result) => {
            let newSensorList = result.map(result => { return result.sensor });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});


// Doubt API
// Sensors by location
router.get('/location/:location', async (req, res) => {
    try {
        let location = decodeURIComponent(req.params.location);
        let result = await Sensor.find({ sensor_location: location });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});


// Sensors for a floor in a building
router.get('/building/:building/floor/:floor', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        Devices.aggregate([
            { "$match": { "building": building, "floor": floor } },
            {
                "$lookup": {
                    "from": "sensors",
                    "localField": "id",
                    "foreignField": "device_id",
                    "as": "sensorArray"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "sensor": "$sensorArray"
                }
            },
            {
                "$unwind": "$sensor"
            }
        ], (err, result) => {
            let newSensorList = result.map(result => { return result.sensor });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Sensors for locations in a building
router.get('/building/:building/location/:location', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let location = decodeURIComponent(req.params.location);
        Devices.aggregate([
            { "$match": { "building": building, "location": location } },
            {
                "$lookup": {
                    "from": "sensors",
                    "localField": "id",
                    "foreignField": "device_id",
                    "as": "sensorArray"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "sensor": "$sensorArray"
                }
            },
            {
                "$unwind": "$sensor"
            }
        ], (err, result) => {
            let newSensorList = result.map(result => { return result.sensor });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Sensors for locations in a floor
router.get('/floor/:floor/location/:location', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        Devices.aggregate([
            { "$match": { "floor": floor, "location": location } },
            {
                "$lookup": {
                    "from": "sensors",
                    "localField": "id",
                    "foreignField": "device_id",
                    "as": "sensorArray"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "sensor": "$sensorArray"
                }
            },
            {
                "$unwind": "$sensor"
            }
        ], (err, result) => {
            let newSensorList = result.map(result => { return result.sensor });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});


// Sensors for location in a floor of a building
router.get('/building/:building/floor/:floor/location/:location', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        Devices.aggregate([
            { "$match": { "building": building, "floor": floor, "location": location } },
            {
                "$lookup": {
                    "from": "sensors",
                    "localField": "id",
                    "foreignField": "device_id",
                    "as": "sensorArray"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "sensor": "$sensorArray"
                }
            },
            {
                "$unwind": "$sensor"
            }
        ], (err, result) => {
            let newSensorList = result.map(result => { return result.sensor });
            res.json(JSON.stringify(newSensorList));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});


// Get sensor info by sensor id
router.get('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        let result = await Sensor.findOne({ _id: id });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
})


module.exports = router;