const express = require('express');
const router = express.Router();
const getData = require('../config/getData');
const PropertyDetails = require('../models/PropertyDetail');
const DvmsDetails = require('../models/Dvmsdetail');

const getPropertyCode = require('../helpers/getPropertyCode');

router.post('/postDetails', async (req, res) => {
    try {
        let newDetails = await new PropertyDetails({
            username: req.body.username,
            property_code: req.body.property_code,
            agent_name: req.body.agent_name,
            agent_id: req.body.agent_id
        });
        newDetails.save();
        res.json({ success: true });
        getPropertyCode()
            .then(result => {
                getData.AgentData();
                getData.AddressData();
                getData.PropertyData();
                getData.UserData();
                getData.DealerData();
                getData.DeviceData();
            })
            .catch(err => console.log(err))
    } catch (e) {
        console.log(e);
        res.json(e);
    }
});

router.get('/getDetails', async (req, res) => {
    try {
        let result = await DvmsDetails.findOne({})
        res.json(result);
    } catch (e) {
        console.log(e);
        res.json(e);
    }
});


router.put('/activate_agent', async (req, res) => {
    try {
        let result = await DvmsDetails.update({ property_code: req.params.property_code }, {
            $set: {
                "username": req.body.username,
                "property_code": req.body.property_code,
                "dvms_id": req.body.dvms_id,
                "activate_agent": true
            }
        })
        res.json(result);
    } catch (e) {
        console.log(e);
        res.json(e);
    }
})

module.exports = router;