const express = require('express');
const router = express.Router();

// Models
const SystemType = require('../models/SystemType');
const Devices = require('../models/Devices');
const Property = require('../models/Property');
const PropertyDetail = require('../models/PropertyDetail');
const Agent = require('../models/Agent');

// List of all systemtypes
router.get('/systemtypes', async (req, res) => {
    try {
        let result = await SystemType.distinct('system_type', { "system_type": { $nin: ["", null] } });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});


// List of all buildings
router.get('/buildings', async (req, res) => {
    try {
        Devices.aggregate([
            { "$match": { "building": { "$ne": null } } },
            { "$group": { _id: "$building", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});


// List of all floors
router.get('/floors', async (req, res) => {
    try {
        Devices.aggregate([
            { "$match": { "floor": { "$ne": null } } },
            { "$group": { _id: "$floor", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});

// List of all locations
router.get('/locations', async (req, res) => {
    try {
        Devices.aggregate([
            { "$match": { "location": { "$ne": null } } },
            { "$group": { _id: "$location", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});


// List all floors of a building
router.get('/building/:building/floors', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        Devices.aggregate([
            { "$match": { "building": building } },
            { "$group": { _id: "$floor", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});

// List all locations of a building
router.get('/building/:building/locations', (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        Devices.aggregate([
            { "$match": { "building": building } },
            { "$group": { _id: "$location", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});


// List all locations of a floor of building
router.get('/building/:building/floor/:floor/locations', (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        Devices.aggregate([
            { "$match": { "building": building, "floor": floor } },
            { "$group": { _id: "$location", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});


// List of all locations for a floor
router.get('/floor/:floor/locations', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        Devices.aggregate([
            { "$match": { "floor": floor } },
            { "$group": { _id: "$location", status: { $addToSet: "$status" } } }
        ], (err, result) => {
            res.json(JSON.stringify(result));
        })
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get property name and agent name
router.get('/propertydetails', async (req, res) => {
    let property_name = await Property.findOne({}, { property_name: 1, _id: 0 });
    let display_name = await Agent.findOne({}, { display_name: 1, _id: 0 });
    let result = {
        property_name,
        display_name
    };
    res.json(result);
})
module.exports = router;