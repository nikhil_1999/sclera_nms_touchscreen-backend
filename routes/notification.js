const express = require('express');
const router = express.Router();
const app = express();


// Models
const Devices = require('../models/Devices');
const Docker = require('../models/Docker');
const Notification = require('../models/Notification');


// Get 25 recent notifications notifications 
router.get('/all', async (req, res) => {
    try {
        let result = await Devices.find({ $and: [{ alarm: { $ne: null } }, { monitor: 1 }] });
        res.json((JSON.stringify(result)));
    } catch (e) {
        res.status(500).json(e);
    }
});

// Get notification of devices for a systemtype
router.get('/systemtype/:system_type', async (req, res) => {
    try {
        let system_type = decodeURIComponent(req.params.system_type);
        await Docker.aggregate([
            { "$match": { "system_type": system_type } },
            {
                "$lookup": {
                    "from": "devices",
                    "localField": "id",
                    "foreignField": "docker_id",
                    "as": "devices"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "devices": "$devices"
                }
            },
            {
                "$unwind": "$devices"
            }
        ], (err, result) => {
            let formattedDeviceList = result.map(dev => { return dev.devices });
            let devicesAlarm = formattedDeviceList.filter(dev => dev.alarm !== null);
            res.json(JSON.stringify(devicesAlarm));
        });
    } catch (e) {
        res.status(500).json(e);
    }
});


// Get notifications of devices for building
router.get('/building/:building', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let result = await Devices.find({ $and: [{ building: building }, { alarm: { $ne: null } }] });
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
});

// Get notifications of devices for a floor
router.get('/floor/:floor', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ $and: [{ floor: floor }, { alarm: { $ne: null } }] })
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
});


// Get notifications of devices for a location
router.get('/location/:location', async (req, res) => {
    try {
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ $and: [{ location: location }, { alarm: { $ne: null } }] })
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
});

// Get notifications of devices for floor in a building
router.get('/building/:building/floor/:floor', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ $and: [{ building: building, floor: floor }, { alarm: { $ne: null } }] })
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
});

// Get  notifications of devices for location in a building
router.get('/building/:building/location/:location', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ $and: [{ building: building, location: location }, { alarm: { $ne: null } }] });
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
})


// Get notifications of devices for a location in a floor in a building
router.get('building/:building/floor/:floor/location/:location', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ $and: [{ building: building, floor: floor, location: location }, { alarm: { $ne: null } }] })
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
});


// Get notifications of devices for a location in a floor
router.get('/floor/:floor/location/:location', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ $and: [{ floor: floor, location: location }, { alarm: { $ne: null } }] });
        res.json(JSON.stringify(result));
    } catch (e) {
        res.status(500).json(e);
    }
});


module.exports = router;