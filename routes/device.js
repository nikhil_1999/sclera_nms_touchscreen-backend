const express = require('express');
const router = express.Router();

// Models
const Devices = require('../models/Devices');
const Dealer = require('../models/Dealer');
const Docker = require('../models/Docker');


//  Get Devices by SystemType
router.get('/systemtype/:system_type', async (req, res) => {
    try {
        const system_type = decodeURIComponent(req.params.system_type);
        Docker.aggregate([
            { "$match": { "system_type": system_type } },
            {
                "$lookup": {
                    "from": "devices",
                    "localField": "id",
                    "foreignField": "docker_id",
                    "as": "devices"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "devices": "$devices"
                }
            },
            {
                "$unwind": "$devices"
            }
        ], (err, result) => {
            let devicelist = result.map(result => { return result.devices });
            res.json(JSON.stringify(devicelist));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get Devices by Building
router.get('/building/:building', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let result = await Devices.find({ building: building });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
})

// Get Devices by Floor
router.get('/floor/:floor', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ floor: floor });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get Devices by Location
router.get('/location/:location', async (req, res) => {
    try {
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ location: location });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get Devices by Building and floor
router.get('/building/:building/floor/:floor', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ building: building, floor: floor });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get Devices by Building and location
router.get('/building/:building/location/:location', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ building: building, location: location });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
})

// Get Devices by Building,Floor and Location
router.get('/building/:building/floor/:floor/location/:location', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ building: building, floor: floor, location: location });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get Devices by Floor and location
router.get('/floor/:floor/location/:location', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ floor: floor, location: location });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get Device By device ID
router.get('/id/:id', async (req, res) => {
    try {
        const { id } = req.params;
        let result = await Devices.findOne({ id: id });
        res.json(result);
    } catch (e) {
        return res.status(500).json(e);
    }
});

//  Get online devices by SystemType
router.get('/systemtype/:system_type/online', async (req, res) => {
    try {
        const system_type = decodeURIComponent(req.params.system_type);
        Docker.aggregate([
            { "$match": { "system_type": system_type } },
            {
                "$lookup": {
                    "from": "devices",
                    "localField": "id",
                    "foreignField": "docker_id",
                    "as": "devices"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "devices": "$devices"
                }
            },
            {
                "$unwind": "$devices"
            }
        ], (err, result) => {
            let formattedDevicelist = result.map(result => { return result.devices });
            let devicelist = formattedDevicelist.filter(device => device.status === 1);
            res.json(JSON.stringify(devicelist));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});

//  Get offline devices by SystemType
router.get('/systemtype/:system_type/offline', async (req, res) => {
    try {
        const system_type = decodeURIComponent(req.params.system_type);
        Docker.aggregate([
            { "$match": { "system_type": system_type } },
            {
                "$lookup": {
                    "from": "devices",
                    "localField": "id",
                    "foreignField": "docker_id",
                    "as": "devices"
                }
            }, {
                "$project": {
                    "_id": 0,
                    "devices": "$devices"
                }
            },
            {
                "$unwind": "$devices"
            }
        ], (err, result) => {
            let formattedDevicelist = result.map(result => { return result.devices });
            let devicelist = formattedDevicelist.filter(device => device.status === 0);
            res.json(JSON.stringify(devicelist));
        });
    } catch (e) {
        return res.status(500).json(e);
    }
});


// Get Online devices by Building
router.get('/building/:building/online', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let result = await Devices.find({ building: building, status: 1 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get offline devices by Building
router.get('/building/:building/offline', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let result = await Devices.find({ building: building, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Floor
router.get('/floor/:floor/online', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ floor: floor, status: 1 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Floor
router.get('/floor/:floor/offline', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ floor: floor, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Location
router.get('/location/:location/online', async (req, res) => {
    try {
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ location: location, status: 1 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get offline devices by Location
router.get('/location/:location/offline', async (req, res) => {
    try {
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ location: location, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Building and floor
router.get('/building/:building/floor/:floor/online', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ building: building, floor: floor, status: 1 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get offline devices by Building and floor
router.get('/building/:building/floor/:floor/offline', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let result = await Devices.find({ building: building, floor: floor, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Building and location
router.get('/building/:building/location/:location/online', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ building: building, location: location, status: 1 });
        redis_client.setex(`${building}-${location}-online-device`, 120, JSON.stringify(result));
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
})

// Get offline devices by Building and location
router.get('/building/:building/location/:location/offline', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ building: building, location: location, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Building,Floor and Location
router.get('/building/:building/floor/:floor/location/:location/online', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ building: building, floor: floor, location: location, status: 1 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get offline devices by Building,Floor and Location
router.get('/building/:building/floor/:floor/location/:location/offline', async (req, res) => {
    try {
        let building = decodeURIComponent(req.params.building);
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ building: building, floor: floor, location: location, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get online devices by Floor and location
router.get('/floor/:floor/location/:location/online', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ floor: floor, location: location, status: 1 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});

// Get offline devices by Floor and location
router.get('/floor/:floor/location/:location/offline', async (req, res) => {
    try {
        let floor = decodeURIComponent(req.params.floor);
        let location = decodeURIComponent(req.params.location);
        let result = await Devices.find({ floor: floor, location: location, status: 0 });
        res.json(JSON.stringify(result));
    } catch (e) {
        return res.status(500).json(e);
    }
});



//Get other dealers of a device
router.get('/othervendor/:device_id', async (req, res) => {
    try {
        Devices.find({ id: parseInt(req.params.device_id) }, { other_dealer_account_number: 1 }, async (err, result) => {
            if (result) {
                console.log('othervendor', result)
                await Dealer.find({
                    account_number: {
                        $in: result[0].other_dealer_account_number
                    }
                }, (err, dealers) => {
                    if (dealers) {
                        res.json(dealers);
                    }
                    else {
                        res.json(err);
                    }
                }
                )
            }
            else {
                console.log(err)
            }
        });

    } catch (e) {
        return res.status(500).json(e);
    }
});

//Global and Local dealers of a device
router.get('/vendor/:type/:device_id', async (req, res) => {
    try {
        Devices.find({ id: req.params.device_id }, { [`${req.params.type}`]: 1 }, async (err, result) => {
            if (result) {
                console.log('local', result[0][`${req.params.type}`])
                await Dealer.find({
                    account_number: result[0][`${req.params.type}`]
                }, (err, dealers) => {
                    if (dealers) {
                        res.json(dealers);
                    }
                    else {
                        res.json(err);
                    }
                }
                )
            }
            else {
                console.log(err)
            }
        });

    } catch (e) {
        return res.status(500).json(e);
    }
});



// Get dealers and device details for dashboard
router.get('/dashboard', async (req, res) => {
    try {
        let result = await Devices.find({ $and: [{ monitor: 1 },{dashboard_status:{$ne:null}}] });
        res.json(result);
    } catch (e) {
        return res.status(500).json(e);
    }
});


// Update dashboard status based on device id
router.put('/updatedashboard/:id/:status', async (req, res) => {
    try {
        console.log('updatedashboard', req.body)
        let result = await Devices.findOneAndUpdate({ id: req.params.id }, {
            $set: {
                "dashboard_status": req.params.status,
                "ticket": req.body.ticket
            }
        })
        res.json(result);
    } catch (e) {
        console.log(e);
        res.json(e);
    }
})



module.exports = router;