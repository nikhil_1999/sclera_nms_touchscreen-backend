const express = require('express');
const router = express.Router();

const User = require('../models/User');

router.get('/systemtype/:system_type/mastervendor',async(req,res)=>{
    try{
        let system_type = decodeURIComponent(req.params.system_type);
        let result = await User.findOne({system_types:system_type});
        res.json(JSON.stringify(result));
    }catch(e){
        res.status(500).json(e);
    }
});

module.exports = router;