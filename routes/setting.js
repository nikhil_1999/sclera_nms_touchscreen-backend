const express = require('express');
const router = express.Router();
const { exec } = require('child_process');

// Models
const ApprovedVendor = require('../models/ApprovedVendor');
const SystemInfo = require('../models/SystemInfo');


// Get docker details
router.get('/getdockers', async (req, res) => {
    try {
        let result = await ApprovedVendor.find({});
        res.json(result);
    } catch (e) {
        res.status(500).json(e);
    }
});

// Insert docker details
router.post('/updatedocker/vendor/:vendor_id/systemtype/:system_type/:id', async (req, res) => {
    try {
        exec('docker run -d ubuntu /bin/bash', (err, stdout, stderr) => {
            if (err) {
                console.log(`${stderr}`);
            }
            else {
                var docker_id = stdout.trim('\n');
                req.body['docker_id'] = docker_id;

                ApprovedVendor.update({ $and: [{ vendor_id: req.params.vendor_id }, {_id:req.params.id}, { system_type: req.params.system_type }] }, {
                    $set: req.body
                }, (err, result) => {
                    res.json(result)
                })
            }
        });
    }
    catch (e) {
       res.status(500).json(e);
    }

});

// Insert docker details when docker id is known
router.post('/updatedocker/vendor/:vendor_id/systemtype/:system_type/docker/:docker_id', async (req, res) => {
    try {
        ApprovedVendor.update({ $and: [{ vendor_id: req.params.vendor_id }, { system_type: req.params.system_type }, { docker_id: req.params.docker_id }] }, {
            $set: req.body
        }, (err, result) => {
            res.json(result)
        })
    }
    catch (e) {
       res.status(500).json(e);
    }

});

// Send system details
router.get('/get/systemdetails',async(req,res)=>{
    try{    
        let result = await SystemInfo.findOne({});
        res.json(result);
    }catch(e){
        res.status(500).error(e);
    }
});

router.post('/insert/systemdetails/',async(req,res)=>{
    try{
        let result = new SystemInfo({
            vlan_id:req.body.vlan_id,
            interface:req.body.interface,
            dhcp:req.body.dhcp,
            tagged:req.body.tagged,
            primary_dns:req.body.primary_dns,
            mac_address:req.body.mac_address,
            secondary_dns:req.body.secondary_dns,
            ip:req.body.ip,
            prefix:req.body.prefix,
            gateway:req.body.gateway,
            active:req.body.active,
            static:req.body.static,
            configured:req.body.configured
        }).save();

        res.json({success:true});
    }catch(e){
        res.status(500).error(e);
    }
})

module.exports = router;
