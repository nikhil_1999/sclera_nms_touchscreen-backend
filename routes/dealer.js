const express = require('express');
const router = express.Router();

const Dealer = require('../models/Dealer');

// List of all dealers (Used in contact page)
router.get('/all', async (req, res) => {
    try {
        let result = await Dealer.find({}).sort({ 'dealer_name': 1 });
        res.json(result);
    } catch (e) {
        console.log(e);
        res.json(e);
    }
});

module.exports = router;

