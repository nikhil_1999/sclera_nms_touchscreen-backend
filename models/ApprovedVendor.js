const mongoose = require('mongoose');

const ApprovedVendorSchema = new mongoose.Schema({
    vendor_id:{
        type:String,
        required:true
    },
    system_type:{
        type:String,
        required:true
    },
    docker_id:{
        type:String
    },
    agent_id:{
        type:String
    },
    vlan_id:{
        type:Number
    },
    interface:{
        type:String
    },
    dhcp:{
        type:Boolean
    },
    tagged:{
        type:Boolean
    },
    primary_dns:{
        type:String
    },
    secondary_dns:{
        type:String
    },
    ip:{
        type:String
    },
    prefix:{
        type:Number
    },
    gateway:{
        type:String
    },
    active:{
        type:Boolean
    },
    static:{
        type:Boolean
    }
});

module.exports = mongoose.model('ApprovedVendor',ApprovedVendorSchema);