const mongoose = require('mongoose');

const SensorSchema = new mongoose.Schema({
    sensor_id: {
        type: String
    },
    sensor_name: {
        type: String
    },
    user_data_sensor_name: {
        type: String
    },
    sensor_des: {
        type: String
    },
    user_data_sensor_des:{
        type:String
    },
    sensor_uplimit: {
        type: Number
    },
    sensor_lowlimit: {
        type: Number
    },
    sensor_value: {
        type: Number
    },
    sensor_unit:{
        type:String
    },
    sensor_status: {
        type: String
    },
    sensor_location:{
        type:String
    },
    device_id:{
        type:String
    },
    sensor_type:{
        type:String
    }
});

module.exports = mongoose.model('Sensor', SensorSchema);