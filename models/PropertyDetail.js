const mongoose = require('mongoose');

const PropertyDetailsSchema = new mongoose.Schema({
    username:{
        type:String,
        required:true
    },
    property_code:{
        type:String,
        required:true
    },
    agent_name:{
        type:String,
        required:true
    },
    agent_id:{
        type:String,
        required:true
    },
    publicIp:{
        type:String
    }
});

module.exports = mongoose.model('PropertyDetails',PropertyDetailsSchema);