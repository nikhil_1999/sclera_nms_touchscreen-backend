const mongoose = require('mongoose');

const AddressSchema = new mongoose.Schema ({
    id:{
        type:String
    },
    address:{
        type:String
    }
});

module.exports = mongoose.model('Address',AddressSchema);