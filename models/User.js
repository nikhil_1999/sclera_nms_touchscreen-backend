const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    username: { type: String },
    extension: { type: String },
    option: { type: String },
    name: { type: String },
    email: { type: String },
    phone: { type: String },
    company_name: { type: String },
    website: { type: String },
    address: { type: String },
    system_types: { type: Array },
    property_property_code: {
        type: String
    },
    street: {
        type: String
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    zip: {
        type: String
    },
    country: {
        type: String
    }
});

module.exports = mongoose.model('User', UserSchema); 