const mongoose = require('mongoose');

const NotificationsSchema = new mongoose.Schema({
    id: {
        type: String
    },
    docker_id: {
        type: String
    },
    display_name: {
        type: String
    },
    history: {
        type: String
    },
    hw_address: {
        type: String
    },
    ip_addresses: {
        type: String
    },
    last_seen_on: {
        type: String
    },
    location: {
        type: String
    },
    model: {
        type: String
    },
    network_layer: {
        type: String
    },
    notes: {
        type: String
    },
    type: {
        type: String
    },
    user_data_model: {
        type: String
    },
    user_data_name: {
        type: String
    },
    user_data_type: {
        type: String
    },
    user_data_vendor: {
        type: String
    },
    vendor: {
        type: String
    },
    agent_id: {
        type: String
    },
    building: {
        type: String
    },
    floor: {
        type: String
    },
    global_dealer_account_number: {
        type: String
    },
    local_dealer_account_number: {
        type: Object
    },
    other_dealer_account_number: {
        type: []
    },
    parent_device_id: {
        type: String
    },
    important: {
        type: Number
    },
    monitor: {
        type: Number
    },
    status: {
        type: Number
    },
    system_type: {
        type: String
    },
    alarm: {
        type: String
    },
    seen: {
        type: String
    },
    dashboard_status: {
        type: String
    },
    ticket: {
        type: String
    },
    notification_type: {
        type: Number
    },
    notification_timestamp:{
        type:String
    }
});

module.exports = mongoose.model('Notification', NotificationsSchema);


