const mongoose = require('mongoose');

const PropertySchema = new mongoose.Schema({
  property_code: {
      type:String,
      required:true
  },
  account_number:{
      type:String
  },
  email:{
    type:String,
    required:true
  }, 
  phone:{
      type:String,
      required:true
  },
  extension:{
      type:String
  },
  option:{
    type:String
  },
  property_name:{
    type:String,
    required:true
  },
  website: {
      type:String
  },
  address_id:{
    type:String,
    required:true
  },
  user_username:{
      type:String,
      required:true
  } 
});

module.exports = mongoose.model('Property',PropertySchema);
