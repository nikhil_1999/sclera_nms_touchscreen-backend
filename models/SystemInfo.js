const mongoose = require('mongoose');

const SystemInfoSchema = new mongoose.Schema({
    vlan_id:{
        type:Number
    },
    interface:{
        type:String
    },
    dhcp:{
        type:Boolean
    },
    mac_address:{
        type:String
    },
    tagged:{
        type:Boolean
    },
    primary_dns:{
        type:String
    },
    secondary_dns:{
        type:String
    },
    ip:{
        type:String
    },
    prefix:{
        type:Number
    },
    gateway:{
        type:String
    },
    active:{
        type:Boolean
    },
    static:{
        type:Boolean
    },
    configured:{
        type:Boolean,
        default:false
    }
});

module.exports = mongoose.model('SystemInfo',SystemInfoSchema);