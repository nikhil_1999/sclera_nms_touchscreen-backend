const mongoose = require('mongoose');

const AgentSchema = new mongoose.Schema ({
    id:{
        type:String,
        required:true,
    },
    display_name:{
        type:String,
    },
    status:{
        type:Boolean,
    },
    timezone:{
        type:String
    },
    address_id:{
        type:String
    },
    property_property_code:{
        type:String,
        required:true
    },
    ip_address:{
        type:String
    },
    mac_address:{
        type:String
    }
});

module.exports = mongoose.model('Agent',AgentSchema);





