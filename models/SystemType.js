const mongoose = require('mongoose');

const SystemTypeSchema = new mongoose.Schema({
    id: {
        type: String,

    },
    agent_id: {
        type: String,

    },
    mac: {
        type: String,

    },
    ip: {
        type: String,

    },
    system_type: {
        type: String,

    },
    username: {
        type: String,

    },
    subnet_mask: {
        type: String,

    }
});

module.exports = mongoose.model('SystemType', SystemTypeSchema);