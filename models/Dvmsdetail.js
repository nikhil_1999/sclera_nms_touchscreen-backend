const mongoose = require('mongoose');

const DvmsDetailsSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    property_code: {
        type: String,
        required: true
    },
    dvms_name: {
        type: String

    },
    dvms_id: {
        type: String,
        required: true
    },
    public_ip: {
        type: String
    },
    activate_agent: {
        type: Boolean,
        required: true
    }
});

module.exports = mongoose.model('DvmsDetails', DvmsDetailsSchema);