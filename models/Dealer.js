const mongoose = require('mongoose');

const DealerSchema = new mongoose.Schema({
    "account_number": {
        type: String
    },
    "dealer_name":
    {
        type: String
    },
    "email": {
        type: String
    },
    "phone": {
        type: String
    },
    "extension": {
        type: String
    },
    "option": {
        type: String
    },
    "type": {
        type: String
    },
    "address": {
        type: String
    },
    "property_property_code": {
        type: String
    },
    "street": {
        type: String
    },
    "city": {
        type: String
    },
    "state": {
        type: String
    },
    "zip": {
        type: String
    },
    "country": {
        type: String
    }
});

module.exports = mongoose.model('Dealer', DealerSchema);


