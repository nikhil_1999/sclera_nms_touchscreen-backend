const DvmsDetails = require('../models/Dvmsdetail');

async function getPropertyCode(){
    property_code = await DvmsDetails.findOne({},{'property_code':1})
    return property_code;
}

module.exports = getPropertyCode;