const { spawn,exec } = require('child_process');
const Devices = require('../models/Devices');
const oui = require('oui');

async function updateDeviceData(){
    // Get all devices from collection
    let devices = await Devices.find({});

    for(let i=0;i<devices.length;i++){
        // Fill device display name using avahi resolve
        if(devices[i].display_name === null){
            exec(`avahi-resolve -a ${devices[i].ip_addresses}`,async(err,stdout,stderr)=>{
                if(!stderr){ 
                    devices[i].display_name = stdout.split(`\t`)[1].split(`.`)[0];
                }else{
                    devices[i].display_name = devices[i].hw_address;
                }
                await Devices.updateMany({ip_addresses:devices[i].ip_addresses},{$set:{display_name:devices[i].display_name}});
            });
        }

        // Fill device vendor name using oui
            let vendor = (oui(JSON.stringify(devices[i].hw_address)));
            if(vendor!==null){
                devices[i].vendor = vendor.split(`\n`)[0];
                await Devices.updateMany({hw_address:devices[i].hw_address},{$set:{vendor:devices[i].vendor}});
            }
    }
}

module.exports = updateDeviceData;