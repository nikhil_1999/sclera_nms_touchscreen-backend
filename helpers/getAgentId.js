const Dvmsdetails = require('../models/Dvmsdetail');

async function getAgentId(){
    let dvms_id = await Dvmsdetails.findOne({},{'dvms_id':1,'_id':0})
    return dvms_id;
}

module.exports = getAgentId;