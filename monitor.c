//gcc -o monitor $(mysql_config --cflags) monitor.c $(mysql_config --libs) -lpthread

//gcc -I /home/avinash/arp1/ws/example/..//include -o second $(mysql_config --cflags) second.c $(mysql_config --libs) -lpthread /home/avinash/arp1/ws/example/..//libws.a

//gcc moni.c -o moni -lpthread $(pkg-config --cflags --libs libmongoc-1.0) -lcurl

#include <arpa/inet.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h> // Exit(1)
#include <math.h>
//#include <ws.h>
#include <unistd.h> // getuid() and getuid()

#include <sys/ioctl.h>
#include <bits/ioctls.h>   
#include <net/if.h>
#include <errno.h>
#include <time.h>     
#include <stdbool.h>
#include<pthread.h>
#include <signal.h>
//#include <sys/types.h>


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset(), and memcpy()
 
#include <netdb.h>            // struct addrinfo
#include <sys/types.h>        // needed for socket(), uint8_t, uint16_t
#include <sys/socket.h>       // needed for socket()
#include <netinet/in.h>       // IPPROTO_RAW, INET_ADDRSTRLEN
#include <netinet/ip.h>       // IP_MAXPACKET (which is 65535)
#include <arpa/inet.h>        // inet_pton() and inet_ntop()
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <linux/if_ether.h>   // ETH_P_ARP = 0x0806
#include <linux/if_packet.h>  // struct sockaddr_ll (see man 7 packet)
#include <net/ethernet.h>
 
#include <errno.h>            // errno, perror()


#include <bson/bson.h>
#include <mongoc/mongoc.h>
#include <stdio.h>
#include <curl/curl.h>

struct LocalNetwork
{
    in_addr_t ip, mask, netid, broadcast_ip;
}local_network;

struct Local
{
    in_addr_t ip;
    in_addr_t subnet;
    uint8_t mac[6];
    char *interface;
}local;

struct DeviceInfo
{
    bool status;
    char vendor[30];
    time_t last_status_changed;
};

struct ArpTable
{
    in_addr_t ip;
    uint8_t mac[6];
    int status;
    int flag;
    struct ArpTable *next;
}*arp_table;



int str2mac(const char* mac, uint8_t* values){
    if( 6 == sscanf( mac, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",&values[0], &values[1], &values[2],&values[3], &values[4], &values[5] ) ){
        return 1;
    }else{
        return 0;
    }
}

int getInterface ();
char* inet_ntoanew(in_addr_t);
char* getMac(char* );
void cpyMacArray(uint8_t *, uint8_t* );
void displayMac(uint8_t *);
int ip2index(uint32_t , uint32_t);
uint32_t index2ip(int, uint32_t);
void ipList();
uint32_t byte2ip(uint8_t *);
uint8_t *ip2byte(uint8_t *, uint32_t);
void * receive_arp( void * );
void genScan();
int genScanarpReceive();
void * genScanreceiveArp( void * );
int removeNode(uint8_t *, int);
int cmprMac(uint8_t *, uint8_t *);
int connect2database();
void insertLog(char*, char *, char*, char*, char*);
void mac2string(uint8_t* , char *);
void inserOrUpdateDevice(char*, char *, int, char*);
void inserOrUpdateDcoker(char*, char *, char*, char*);
void connect2MongoDatabase();
void mongoinserOrUpdateDcoker(char *, char *, char *, char *);
void MongoinsertLog(char *, char *, char *, char* , char* );
void mongoinserOrUpdateDevice(char*, char *, int, char*, char *);
void * send_notification( void *ptr );
void mongoinserOrUpdateStatusChange(char *, char *, char *);

int pushNotification(char *, char *, char *);
void send_noti();


/***************************************************************************************************************************************
                                                       ARP
******************************************************************************************************************************************/


typedef struct _arp_hdr arp_hdr;
struct _arp_hdr {
  uint16_t htype;
  uint16_t ptype;
  uint8_t hlen;
  uint8_t plen;
  uint16_t opcode;
  uint8_t sender_mac[6];
  uint8_t sender_ip[4];
  uint8_t target_mac[6];
  uint8_t target_ip[4];
};

// Define some constants.
#define ETH_HDRLEN 14      // Ethernet header length
#define IP4_HDRLEN 20      // IPv4 header length
#define ARP_HDRLEN 28      // ARP header length
#define ARPOP_REQUEST 1    // Taken from <linux/if_arp.h>
#define ARPOP_REPLY 2         // Taken from <linux/if_arp.h>

bool scan_flag = 0;
struct ArpTable *lastnode;

char AGENT_ID[30];
char SYSTEM_TYPE[30];
char MASTER_VENDOR[30];
mongoc_client_t *client;

// Function prototypes
char *allocate_strmem (int);
uint8_t *allocate_ustrmem (int);
int arpSend(uint8_t*, uint8_t*, uint8_t *, uint8_t *);
int arpReceive();


#define BUFSIZE (sizeof(long) * 8 + 1)

char *ltoa(long N, char *str, int base)
{
      register int i = 2;
      long uarg;
      char *tail, *head = str, buf[BUFSIZE];

      if (36 < base || 2 > base)
            base = 10;                    /* can only use 0-9, A-Z        */
      tail = &buf[BUFSIZE - 1];           /* last character position      */
      *tail-- = '\0';

      if (10 == base && N < 0L)
      {
            *head++ = '-';
            uarg    = -N;
      }
      else  uarg = N;

      if (uarg)
      {
            for (i = 1; uarg; ++i)
            {
                  register ldiv_t r;

                  r       = ldiv(uarg, base);
                  *tail-- = (char)(r.rem + ((9L < r.rem) ?
                                  ('A' - 10L) : '0'));
                  uarg    = r.quot;
            }
      }
      else  *tail-- = '0';

      memcpy(head, ++tail, i);
      return str;
}

// int FD;
// void onopen(int fd)
// {
//     FD = fd;
// 	char *cli;
// 	cli = ws_getaddress(fd);
// 	printf("Connection opened, client: %d | addr: %s\n", fd, cli);
// 	free(cli);
// }

// void onclose(int fd)
// {
//     FD = fd;
// 	char *cli;
// 	cli = ws_getaddress(fd);
// 	printf("Connection closed, client: %d | addr: %s\n", fd, cli);
// 	free(cli);
// }


// void onmessage(int fd, unsigned char *msg)
// {
//     FD = fd;
// 	char *cli;
// 	cli = ws_getaddress(fd);
// 	printf("I receive a message: %s, from: %s/%d\n", msg, cli, fd);

// 	ws_sendframe(fd, "I receive:");
// 	ws_sendframe(fd, (char *)msg);
	
// 	free(cli);
// 	free(msg);
// }      


/***************************************************************************************************************************************
                                                       ARP
******************************************************************************************************************************************/



void checkHostName(int hostname)
{
 if (hostname == -1)
 {
 perror("gethostname");
 exit(1);
 }
}
 
// Returns host information corresponding to host name
void checkHostEntry(struct hostent * hostentry)
{
 if (hostentry == NULL)
 {
 perror("gethostbyname");
 exit(1);
 }
}
 
// Converts space-delimited IPv4 addresses
// to dotted-decimal format
void checkIPbuffer(char *IPbuffer)
{
 if (NULL == IPbuffer)
 {
 perror("inet_ntoa");
 exit(1);
 }
}
char* getHostname()
{
 static char hostbuffer[256];
 //tmp = (char *) malloc (len * sizeof (char));
 char *IPbuffer;
 struct hostent *host_entry;
 int hostname;
 
 // To retrieve hostname
 hostname = gethostname(hostbuffer, sizeof(hostbuffer));
 checkHostName(hostname);
 
 printf("Hostname: %s\n", hostbuffer);
 return hostbuffer;
}
char *hostname;

int main(int argc, char *argv[]){
    if (geteuid() || getuid())
    {
        printf("ERROR: You must be root to use this Scanner\n");
        exit(1);
    }
    if(argc < 2)
    {
        printf("Please specify a hostname \n");
        exit(1);
    }
    else if( argc >5)
    {
        printf("Wrong Input!!!!!!!!!!! \n");
        exit(1);
    }
    strcpy(AGENT_ID, argv[2]);
    strcpy(MASTER_VENDOR, argv[3]);
    strcpy(SYSTEM_TYPE, argv[4]);

    if( inet_addr( argv[1] ) != -1) //for taget in ip ex: 192.168.2.1
    {
        local_network.ip=inet_addr(argv[1]);
    }
    else //if target in [IP]/[MASK] or domain name
    {
        char *cird[2];
        cird[1] = "24";
        int i = 0;
        while( (cird[i++] = strsep(&argv[1],"/")) != NULL )
        {
            //printf("%s\n",found[i-1]);
        }
        if(i > 3){
            printf("Wrong Input!!!!!!!!!!! \n");
            exit(1);
        }
        else
        {
            if( inet_addr( cird[0] ) == -1) //for taget in ip ex: 192.168.2.1
            {
                printf("Wrong IP address!!\n");
                exit(1);
            }
            if(atoi(cird[1])>32 ||(atoi(cird[1])<0))
            {
                printf("Wrong Subnet!!\n");
                exit(1);
            }
            else{
                local_network.ip=inet_addr(cird[0]);
                local_network.mask = ntohl((0xFFFFFFFF << (32 - atoi(cird[1]))) & 0xFFFFFFFF);
            }
            
        }
        
    }
    local_network.netid = local_network.ip & local_network.mask;
    local_network.broadcast_ip = local_network.ip | ~local_network.mask;
    if(getInterface()==0)
    {
        printf("interface not found");
        return 0;
    }

    //Variables
    hostname = getHostname();
    
    int total_ip;

    total_ip = ip2index(local_network.broadcast_ip, local_network.mask);
    // strcpy(local.mac,getMac(local.interface));
    memcpy (local.mac,getMac(local.interface), 6 * sizeof (uint8_t));
 
    printf("****************************************************************************************\n");
    printf("                            Device's Interface Information:\n");
    printf("****************************************************************************************\n");
    printf("Interface Name: %s\n",local.interface);
    printf("Interface Mac Address: ");
    displayMac(local.mac);
    printf("Interface IP Address: %s\n",inet_ntoanew(local.ip));
    printf("Subnet Mask: %s\n",inet_ntoanew(local.subnet));
    printf("****************************************************************************************\n");
    printf("                                  Network Information:\n");
    printf("****************************************************************************************\n");
    printf("Subnet Mask: %s\n",inet_ntoanew(local_network.mask));
    printf("Netid: %s\n",inet_ntoanew(local_network.netid));
    printf("Broadcast IP Address%s\n",inet_ntoanew(local_network.broadcast_ip));
    printf("Total IP addresses: %d\n",total_ip);
    printf("****************************************************************************************\n");
    printf("\n");

    arp_table = (struct ArpTable *) calloc (total_ip , sizeof (struct ArpTable));
    connect2MongoDatabase();

    char stringmac[18];
    mac2string(local.mac, stringmac);
    mongoinserOrUpdateDcoker(hostname, stringmac, inet_ntoanew(local.ip), AGENT_ID);
    
    char *message1 = "Thread 1";
    char *message2 = "Thread 2";
    int  iret1;
    pthread_t recv_thread, arp_thread, notification_thread;

 
    if( pthread_create( &recv_thread , NULL ,  receive_arp , (void*) message1) < 0)
    {
        printf ("Could not create sniffer thread. Error number : %d . Error message : %s \n" , errno , strerror(errno));
        exit(0);
    }

    // if( pthread_create( &notification_thread , NULL , send_notification  , (void*) message2) < 0)
    // {
    //     printf ("Could not create sniffer thread. Error number : %d . Error message : %s \n" , errno , strerror(errno));
    //     exit(0);
    // }

   lastnode =  &arp_table[0];
   lastnode->next = NULL;
    ipList();
    genScan();
    //pthread_kill(recv_thread,1);
    pthread_join( recv_thread , NULL);
    free(local.interface);
    free(arp_table);
    
}


void * receive_arp( void *ptr )
{
    //Start the sniffer thing
    arpReceive();
}

// void * send_notification( void *ptr )
// {
//     //Start the sniffer thing
//     send_noti();
// }

// void send_noti()
// {
//     struct ws_events evs;
// 	evs.onopen    = &onopen;
// 	evs.onclose   = &onclose;
// 	evs.onmessage = &onmessage;
// 	//ws_socket(&evs, 8080);
// }
 

int getInterface ()
{
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa, *ss;

    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next)
     {
        if (ifa->ifa_addr->sa_family==AF_INET) 
        {
            sa = (struct sockaddr_in *) ifa->ifa_addr;
            ss = (struct sockaddr_in *) ifa->ifa_netmask;
            in_addr_t add, mask;
            add= sa->sin_addr.s_addr;
            mask = ss->sin_addr.s_addr;
            if((add & mask) == local_network.netid)
            {
               local.interface = (char*)malloc(strlen(ifa->ifa_name) * sizeof(char));
               strcpy(local.interface,ifa->ifa_name );
               local.ip = sa->sin_addr.s_addr;
               local.subnet = ss->sin_addr.s_addr;
               return 1;
               
            }
        }
    }
    freeifaddrs(ifap);
    return 0;
}

char* inet_ntoanew(in_addr_t val){
    struct in_addr value;
    value.s_addr = val;
    return(inet_ntoa(value));
}


// return mac address in static type
char* getMac(char* interface)
{
    int sd;
    struct ifreq ifr;
    if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
    perror ("socket() failed to get socket descriptor for using ioctl() ");
    }
 
  // Use ioctl() to look up interface name and get its MAC address.
  memset (&ifr, 0, sizeof (ifr));
  snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", interface);
  if (ioctl (sd, SIOCGIFHWADDR, &ifr) < 0) {
    perror ("ioctl() failed to get source MAC address ");
  }
  close (sd);
  static uint8_t tempmac[6];
  memcpy (tempmac, ifr.ifr_hwaddr.sa_data, 6 * sizeof (uint8_t));
  return tempmac;
}

void displayMac(uint8_t *macadd)
{
    int i = 0;
    for (i=0; i<5; i++) {
    printf ("%02x:", macadd[i]);
    }
    printf ("%02x\n", macadd[5]);
}


int ip2index(uint32_t ip, uint32_t mask)
{
    return ntohl(ip & ~mask);
}

uint32_t index2ip(int index, uint32_t netid)
{
    uint32_t val = index;
    return(htonl(val) | netid);
}
void ipList()
{

    uint32_t ip;
    uint8_t sender_ip[4], target_ip[4], target_mac[6], *sender_mac;
    str2mac("ff:ff:ff:ff:ff:ff",target_mac);
    sender_mac = local.mac;
    ip2byte(sender_ip,local.ip);
    ip = ntohl(local_network.netid);
    ip++;
    ip2byte(target_ip,htonl(ip));

    //Arp starts here
    int i, status, frame_length, sd, bytes;
    arp_hdr arphdr;
    uint8_t *ether_frame;
    struct sockaddr_ll device;
    //struct ifreq ifr;
    
    // Allocate memory for various arrays.
    ether_frame = allocate_ustrmem (IP_MAXPACKET);
    
    // Find interface index from interface name and store index in
    // struct sockaddr_ll device, which will be used as an argument of sendto().
    memset (&device, 0, sizeof (device));
    if ((device.sll_ifindex = if_nametoindex (local.interface)) == 0) {
        perror ("if_nametoindex() failed to obtain interface index ");
        //return -1;
    }

 
    // Fill out sockaddr_ll.
    device.sll_family = AF_PACKET;
    memcpy (device.sll_addr, sender_mac, 6 * sizeof (uint8_t));
    device.sll_halen = 6;
    
    // ARP header
    
    // Hardware type (16 bits): 1 for ethernet
    strcpy(arphdr.sender_ip, sender_ip);
    
    
    arphdr.htype = htons (1);
    
    // Protocol type (16 bits): 2048 for IP
    arphdr.ptype = htons (ETH_P_IP);
    
    // Hardware address length (8 bits): 6 bytes for MAC address
    arphdr.hlen = 6;
    
    // Protocol address length (8 bits): 4 bytes for IPv4 address
    arphdr.plen = 4;
    
    // OpCode: 1 for ARP request
    //arphdr.opcode = htons (ARPOP_REQUEST);

    arphdr.opcode = htons (ARPOP_REQUEST);

    
    // Sender hardware address (48 bits): MAC address
    memcpy (&arphdr.sender_mac, sender_mac, 6 * sizeof (uint8_t));


    
    // Target hardware address (48 bits): zero, since we don't know it yet.
    memset (&arphdr.target_mac, 0, 6 * sizeof (uint8_t));
    
    // Target protocol address (32 bits)
    // See getaddrinfo() resolution of target.
    
    // Fill out ethernet frame header.
    
    // Ethernet frame length = ethernet header (MAC + MAC + ethernet type) + ethernet data (ARP header)
    frame_length = 6 + 6 + 2 + ARP_HDRLEN;
    
    // Destination and Source MAC addresses
    memcpy (ether_frame, target_mac, 6 * sizeof (uint8_t));
    memcpy (ether_frame + 6, sender_mac, 6 * sizeof (uint8_t));
    
    // Next is ethernet type code (ETH_P_ARP for ARP).
    // http://www.iana.org/assignments/ethernet-numbers
    ether_frame[12] = ETH_P_ARP / 256;
    ether_frame[13] = ETH_P_ARP % 256;

    if ((sd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0) {
        perror ("socket() failed ");
    }

    printf("Network Scanning Started.......\n");
    scan_flag = 1;
    while(htonl(ip)!=local_network.broadcast_ip)
    {
        strcpy(arphdr.target_ip, target_ip);
        memcpy (ether_frame + ETH_HDRLEN, &arphdr, ARP_HDRLEN * sizeof (uint8_t));
        
        // Send ethernet frame to socket.
        if ((bytes = sendto (sd, ether_frame, frame_length, 0, (struct sockaddr *) &device, sizeof (device))) <= 0) {
            perror ("sendto() failed");
        }
        ip++;
        ip2byte(target_ip,htonl(ip));
        usleep(200000);
    }      

   close (sd);
   free (ether_frame);
   sleep(2);
   
   scan_flag = 0;
   int count = 1;
   struct ArpTable *temp;
   temp = &arp_table[0];
   temp= temp->next;
   printf("****************************************************************************************\n");
   printf("List of Online Devices:\n");
   printf("****************************************************************************************\n");
   if(temp!=NULL)
   {
        while(temp->next!=NULL)
        {     
            //ws_sendframe(FD, inet_ntoanew(temp->ip));
            printf("....................................................................................\n");
            printf("IP Address: %s          ", inet_ntoanew(temp->ip));
            printf("Mac Address: ");
            displayMac(temp->mac);
            printf("....................................................................................\n");
            temp= temp->next;
            count++;
            
        }
        //ws_sendframe(FD, inet_ntoanew(temp->ip));
        printf("....................................................................................\n");
        printf("IP Address: %s          ", inet_ntoanew(temp->ip));
        printf("Mac Address: ");
        displayMac(temp->mac);
        printf("....................................................................................\n");
        count++;
   }
   //ws_sendframe(FD, inet_ntoanew(local.ip));
    printf("....................................................................................\n");
    printf("IP Address: %s          ", inet_ntoanew(local.ip));
    printf("Mac Address: ");
    displayMac(local.mac);
    printf("....................................................................................\n");
    printf("Total Online Devices Found: %d\n",count);
}



uint32_t byte2ip(uint8_t *byteip)
{
    return (byteip[0] | (byteip[1] << 8) | (byteip[2] << 16) | (byteip[3] << 24));
}

uint8_t *ip2byte(uint8_t tempbytes[4],uint32_t ip)
{
    tempbytes[0] = ip;
    tempbytes[3] = (ip >> 24);
    tempbytes[2] = (ip >> 16);
    tempbytes[1] = (ip >> 8);
    
    return tempbytes;
}


/********************************************************************************************************
                                                 ARP 
********************************************************************************************************/

// Allocate memory for an array of chars.
char *
allocate_strmem (int len)
{
  void *tmp;
 
  if (len <= 0) {
    fprintf (stderr, "ERROR: Cannot allocate memory because len = %i in allocate_strmem().\n", len);
    exit (EXIT_FAILURE);
  }
 
  tmp = (char *) malloc (len * sizeof (char));
  if (tmp != NULL) {
    memset (tmp, 0, len * sizeof (char));
    return (tmp);
  } else {
    fprintf (stderr, "ERROR: Cannot allocate memory for array allocate_strmem().\n");
    exit (EXIT_FAILURE);
  }
}
 
// Allocate memory for an array of unsigned chars.
uint8_t *
allocate_ustrmem (int len)
{
  void *tmp;
 
  if (len <= 0) {
    fprintf (stderr, "ERROR: Cannot allocate memory because len = %i in allocate_ustrmem().\n", len);
    exit (EXIT_FAILURE);
  }
 
  tmp = (uint8_t *) malloc (len * sizeof (uint8_t));
  if (tmp != NULL) {
    memset (tmp, 0, len * sizeof (uint8_t));
    return (tmp);
  } else {
    fprintf (stderr, "ERROR: Cannot allocate memory for array allocate_ustrmem().\n");
    exit (EXIT_FAILURE);
  }
}


int arpSend(uint8_t *sender_mac, uint8_t *sender_ip, uint8_t *target_mac, uint8_t *target_ip)
{

  int i, status, frame_length, sd, bytes;
  arp_hdr arphdr;
  uint8_t *ether_frame;
  struct sockaddr_ll device;
  //struct ifreq ifr;
 
  // Allocate memory for various arrays.
  ether_frame = allocate_ustrmem (IP_MAXPACKET);
 
  // Find interface index from interface name and store index in
  // struct sockaddr_ll device, which will be used as an argument of sendto().
  memset (&device, 0, sizeof (device));
  if ((device.sll_ifindex = if_nametoindex (local.interface)) == 0) {
    perror ("if_nametoindex() failed to obtain interface index ");
    return -1;
  }

 
  // Fill out sockaddr_ll.
  device.sll_family = AF_PACKET;
  memcpy (device.sll_addr, sender_mac, 6 * sizeof (uint8_t));
  device.sll_halen = 6;
 
  // ARP header
 
  // Hardware type (16 bits): 1 for ethernet
  strcpy(arphdr.sender_ip, sender_ip);
  strcpy(arphdr.target_ip, target_ip);
  
  arphdr.htype = htons (1);
 
  // Protocol type (16 bits): 2048 for IP
  arphdr.ptype = htons (ETH_P_IP);
 
  // Hardware address length (8 bits): 6 bytes for MAC address
  arphdr.hlen = 6;
 
  // Protocol address length (8 bits): 4 bytes for IPv4 address
  arphdr.plen = 4;
 
  // OpCode: 1 for ARP request
  //arphdr.opcode = htons (ARPOP_REQUEST);

  arphdr.opcode = htons (ARPOP_REQUEST);

 
  // Sender hardware address (48 bits): MAC address
  memcpy (&arphdr.sender_mac, sender_mac, 6 * sizeof (uint8_t));


 
  // Target hardware address (48 bits): zero, since we don't know it yet.
  memset (&arphdr.target_mac, 0, 6 * sizeof (uint8_t));
 
  // Target protocol address (32 bits)
  // See getaddrinfo() resolution of target.
 
 
  // Fill out ethernet frame header.
 
  // Ethernet frame length = ethernet header (MAC + MAC + ethernet type) + ethernet data (ARP header)
  frame_length = 6 + 6 + 2 + ARP_HDRLEN;
 
  // Destination and Source MAC addresses
  memcpy (ether_frame, target_mac, 6 * sizeof (uint8_t));
  memcpy (ether_frame + 6, sender_mac, 6 * sizeof (uint8_t));
 
  // Next is ethernet type code (ETH_P_ARP for ARP).
  // http://www.iana.org/assignments/ethernet-numbers
  ether_frame[12] = ETH_P_ARP / 256;
  ether_frame[13] = ETH_P_ARP % 256;
 
  // Next is ethernet frame data (ARP header).
 
  // ARP header
  memcpy (ether_frame + ETH_HDRLEN, &arphdr, ARP_HDRLEN * sizeof (uint8_t));
 
  // Submit request for a raw socket descriptor.
  if ((sd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0) {
    perror ("socket() failed ");
    //exit (EXIT_FAILURE);
    return -2;
  }
 
  // Send ethernet frame to socket.
  if ((bytes = sendto (sd, ether_frame, frame_length, 0, (struct sockaddr *) &device, sizeof (device))) <= 0) {
    perror ("sendto() failed");
    //exit (EXIT_FAILURE);
    return -3;
  }
 
  // Close socket descriptor.
  close (sd);
 
   free (ether_frame);
  return 1;
}

int arpReceive()
{
  int i, sd, status, index;
  uint8_t *ether_frame;
  arp_hdr *arphdr;
 
  // Submit request for a raw socket descriptor.
  if ((sd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0) {
    perror ("socket() failed ");
    exit (EXIT_FAILURE);
  }
  
  while(1)
  {
    ether_frame = allocate_ustrmem (IP_MAXPACKET);
    arphdr = (arp_hdr *) (ether_frame + 6 + 6 + 2);
  while (((((ether_frame[12]) << 8) + ether_frame[13]) != ETH_P_ARP)) {
    if ((status = recv (sd, ether_frame, IP_MAXPACKET, 0)) < 0) {
      if (errno == EINTR) {
        memset (ether_frame, 0, IP_MAXPACKET * sizeof (uint8_t));
        continue;  // Something weird happened, but let's try again.
      } else {
        perror ("recv() failed:");
        exit (EXIT_FAILURE);
      }
    }
  }

    uint32_t tempid = byte2ip(arphdr->sender_ip);
    if((tempid &(local_network.mask)) == local_network.netid && tempid!=local.ip)
    {

        index = ip2index(byte2ip(arphdr->sender_ip),local_network.mask);
        arp_table[index].flag = 1;
        if(arp_table[index].ip != byte2ip(arphdr->sender_ip))  //New IP condtion
        {
            arp_table[index].ip = byte2ip(arphdr->sender_ip);
            arp_table[index].mac[0]= arphdr->sender_mac[0];
            arp_table[index].mac[1]= arphdr->sender_mac[1];
            arp_table[index].mac[2]= arphdr->sender_mac[2];
            arp_table[index].mac[3]= arphdr->sender_mac[3];
            arp_table[index].mac[4]= arphdr->sender_mac[4];
            arp_table[index].mac[5]= arphdr->sender_mac[5];
            arp_table[index].status = 1;
            lastnode->next=&arp_table[index];
            lastnode = lastnode->next;
            lastnode->next = NULL;
            
            int removed_ip = removeNode(arphdr->sender_mac, index);
            if(removed_ip > 1)  // IP changed
            {
                //IP change
                printf("****************************************************************************************\n");
                printf("Notification:\n");
                printf("****************************************************************************************\n");
                printf("IP Address Changed\n");
                printf("New IP Address: %s     ",inet_ntoanew(arp_table[index].ip));
                printf("Mac Address: ");
                displayMac(arp_table[index].mac);
                printf("OLD IP Address: %s\n",inet_ntoanew(index2ip(removed_ip,local_network.netid)));
                char stringmac[18];
                mac2string(arp_table[index].mac, stringmac);
                mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac, 1, AGENT_ID,hostname);
                MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac, "IP Change", inet_ntoanew(index2ip(removed_ip,local_network.netid)), "00:00:00:00:00:00");
            }
            else
            {
                //New device
                printf("****************************************************************************************\n");
                printf("Notification:\n");
                printf("****************************************************************************************\n");
                printf("New Device Found\n");
                printf("IP Address: %s     ",inet_ntoanew(arp_table[index].ip));
                printf("Mac Address: ");
                displayMac(arp_table[index].mac);
                char stringmac[18];
                mac2string(arp_table[index].mac, stringmac);
                MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac, "NEW DEVICE", "0.0.0.0", "00:00:00:00:00:00");
                mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac, 1, AGENT_ID,hostname);
                mongoinserOrUpdateStatusChange(stringmac, hostname, "2");
                pushNotification(stringmac, hostname, "2");
            }
        }
        else //Not a new IP
        {
            if(cmprMac(arphdr->sender_mac,arp_table[index].mac)!=1) //IP conflict or Mac address changed
            {
                int removed_ip = removeNode(arphdr->sender_mac,index);
                if(arp_table[index].status==1) 
                {
                    //IP conflict
                    if(removed_ip >1 )
                    {
                        //IP change
                        printf("****************************************************************************************\n");
                        printf("Notification:\n");
                        printf("****************************************************************************************\n");
                        printf("IP Conflict & IP change\n");
                        printf("IP Address: %s     ",inet_ntoanew(byte2ip(arphdr->sender_ip)));
                        printf("Mac Address: ");
                        displayMac(arphdr->sender_mac);
                        printf("OLD IP Address: %s\n",inet_ntoanew(index2ip(removed_ip,local_network.netid)));
                        char stringmac[18];
                        mac2string(arp_table[index].mac, stringmac);
                        mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac, 0, AGENT_ID,hostname);
                        MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac, "IP Change and IP conflict", inet_ntoanew(index2ip(removed_ip,local_network.netid)), "00:00:00:00:00:00");
                        mongoinserOrUpdateStatusChange(stringmac, hostname, "IP Change and IP conflict");
                    }
                    else
                    {
                        //New Device
                        printf("****************************************************************************************\n");
                        printf("Notification:\n");
                        printf("****************************************************************************************\n");
                        printf("IP Conflict & New Device\n");
                        printf("IP Address: %s     ",inet_ntoanew(byte2ip(arphdr->sender_ip)));
                        printf("Mac Address: ");
                        displayMac(arphdr->sender_mac);
                        char stringmac[18];    
                        mac2string(arp_table[index].mac, stringmac);
                        MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac, "IP Conflict and NEW DEVICE", "0.0.0.0", "00:00:00:00:00:00");
                        mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac, 0, AGENT_ID, hostname);
                        mongoinserOrUpdateStatusChange(stringmac, hostname, "IP Conflict and NEW DEVICE");
                    }
                }
                else
                {
                    //old device removed from monitoring
                    printf("****************************************************************************************\n");
                    printf("Notification:\n");
                    printf("****************************************************************************************\n");
                    printf("Mac Address changed\n");
                    printf("Older Mac Address: ");
                    displayMac(arp_table[index].mac);
                    char stringmac[18];
                    mac2string(arp_table[index].mac, stringmac);
                    MongoinsertLog("0.0.0.0","00:00:00:00:00:00", "MAC Address Expired", inet_ntoanew(arp_table[index].ip), stringmac);
                    arp_table[index].ip = byte2ip(arphdr->sender_ip);
                    arp_table[index].mac[0]= arphdr->sender_mac[0];
                    arp_table[index].mac[1]= arphdr->sender_mac[1];
                    arp_table[index].mac[2]= arphdr->sender_mac[2];
                    arp_table[index].mac[3]= arphdr->sender_mac[3];
                    arp_table[index].mac[4]= arphdr->sender_mac[4];
                    arp_table[index].mac[5]= arphdr->sender_mac[5];
                    arp_table[index].status = 1;
                    if(removed_ip > 1)
                    {
                        //IP change
                        printf("****************************************************************************************\n");
                        printf("Notification:\n");
                        printf("****************************************************************************************\n");
                        printf("IP Address Changed\n");
                        printf("New IP Address: %s     ",inet_ntoanew(arp_table[index].ip));
                        printf("New Mac Address: ");
                        displayMac(arp_table[index].mac);
                        printf("OLD IP Address: %s\n",inet_ntoanew(index2ip(removed_ip,local_network.netid)));
                        char stringmac1[18];
                        mac2string(arp_table[index].mac, stringmac1);
                        mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac1, 1, AGENT_ID,hostname);
                        MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac1, "IP Change & Mac change", inet_ntoanew(index2ip(removed_ip,local_network.netid)), stringmac);
                        mongoinserOrUpdateStatusChange(stringmac, hostname, "IP Change");
                        
                    }
                    else
                    {
                        //New Device
                        printf("****************************************************************************************\n");
                        printf("Notification:\n");
                        printf("****************************************************************************************\n");
                        printf("New Device found\n");
                        printf("IP Address: %s     ",inet_ntoanew(arp_table[index].ip));
                        printf("New Mac Address: ");
                        displayMac(arp_table[index].mac);
                        char stringmac1[18];
                        mac2string(arp_table[index].mac, stringmac1);
                        mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac1, 1, AGENT_ID,hostname);
                        MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac1, "NEW Device & Mac change", "0.0.0.0", stringmac);
                        mongoinserOrUpdateStatusChange(stringmac, hostname, "2");
                        pushNotification(stringmac, hostname, "2" );
                        
                    }
                }
            }
            else
            {
                //Online
                if(arp_table[index].status==1)
                {
                    if(cmprMac(arphdr->sender_ip, arphdr->target_ip)==1) //Graditus arp
                    {
                        //Came Online Quickly
                        printf("****************************************************************************************\n");
                        printf("Notification:\n");
                        printf("****************************************************************************************\n");
                        printf("Device Came Online\n");
                        printf("New IP Address: %s     ",inet_ntoanew(arp_table[index].ip));
                        printf("Mac Address: ");
                        displayMac(arp_table[index].mac);
                        char stringmac[18];
                        mac2string(arp_table[index].mac, stringmac);
                        mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac, 1, AGENT_ID,hostname);
                        MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac, "WENT OFFLINE & Came ONLINE", "0.0.0.0", "00:00:00:00:00:00");
                        mongoinserOrUpdateStatusChange(stringmac, hostname, "7");
                        pushNotification(stringmac, hostname, "7" );
                    }
                    else
                    {
                        //Still ONLINE
                    }
                }
                else
                {
                    //Came ONLINE
                    printf("****************************************************************************************\n");
                    printf("Notification:\n");
                    printf("****************************************************************************************\n");
                    printf("Device Came Online\n");
                    printf("New IP Address: %s     ",inet_ntoanew(arp_table[index].ip));
                    printf("Mac Address: ");
                    displayMac(arp_table[index].mac);
                    arp_table[index].status=1;
                    char stringmac[18];
                    mac2string(arp_table[index].mac, stringmac);
                    mongoinserOrUpdateDevice(inet_ntoanew(arp_table[index].ip), stringmac, 1, AGENT_ID,hostname);
                    mongoinserOrUpdateStatusChange(stringmac, hostname, "8");
                    MongoinsertLog(inet_ntoanew(arp_table[index].ip),stringmac, "Came ONLINE", "0.0.0.0", "00:00:00:00:00:00");
                    pushNotification(stringmac, hostname, "8");
                }
            }
        }
       
    }

  free (ether_frame);
  }
 close (sd);
  
 
  return (EXIT_SUCCESS);
}

void genScan()
{
    uint8_t sender_ip[4], target_ip[4], target_mac[6], *sender_mac;
    struct ArpTable *tempnode = &arp_table[0];
    tempnode = tempnode->next;

    sender_mac = local.mac;
    ip2byte(sender_ip,local.ip);

    //Arp starts here
    int i, status, frame_length, sd, bytes;
    arp_hdr arphdr;
    uint8_t *ether_frame;
    struct sockaddr_ll device;
    //struct ifreq ifr;
    
    // Allocate memory for various arrays.
    ether_frame = allocate_ustrmem (IP_MAXPACKET);
    
    // Find interface index from interface name and store index in
    // struct sockaddr_ll device, which will be used as an argument of sendto().
    memset (&device, 0, sizeof (device));
    if ((device.sll_ifindex = if_nametoindex (local.interface)) == 0) {
        perror ("if_nametoindex() failed to obtain interface index ");
        //return -1;
    }

 
    // Fill out sockaddr_ll.
    device.sll_family = AF_PACKET;
    memcpy (device.sll_addr, sender_mac, 6 * sizeof (uint8_t));
    device.sll_halen = 6;
    
    // ARP header
    
    // Hardware type (16 bits): 1 for ethernet
    strcpy(arphdr.sender_ip, sender_ip);
    
    
    arphdr.htype = htons (1);
    
    // Protocol type (16 bits): 2048 for IP
    arphdr.ptype = htons (ETH_P_IP);
    
    // Hardware address length (8 bits): 6 bytes for MAC address
    arphdr.hlen = 6;
    
    // Protocol address length (8 bits): 4 bytes for IPv4 addressi
    arphdr.plen = 4;
    
    // OpCode: 1 for ARP request
    //arphdr.opcode = htons (ARPOP_REQUEST);

    arphdr.opcode = htons (ARPOP_REQUEST);

    
    // Sender hardware address (48 bits): MAC address
    memcpy (&arphdr.sender_mac, sender_mac, 6 * sizeof (uint8_t));


    
    // Target hardware address (48 bits): zero, since we don't know it yet.
    memset (&arphdr.target_mac, 0, 6 * sizeof (uint8_t));
    
    // Target protocol address (32 bits)
    // See getaddrinfo() resolution of target.
    
    // Fill out ethernet frame header.
    
    // Ethernet frame length = ethernet header (MAC + MAC + ethernet type) + ethernet data (ARP header)
    frame_length = 6 + 6 + 2 + ARP_HDRLEN;
    
    // Destination and Source MAC addresses
    
    memcpy (ether_frame + 6, sender_mac, 6 * sizeof (uint8_t));
    
    // Next is ethernet type code (ETH_P_ARP for ARP).
    // http://www.iana.org/assignments/ethernet-numbers
    ether_frame[12] = ETH_P_ARP / 256;
    ether_frame[13] = ETH_P_ARP % 256;

    if ((sd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0) {
        perror ("socket() failed ");
    }
    while(1)
    {
    tempnode= &arp_table[0];
    tempnode = tempnode->next;
    printf("Network Scanning Started.......\n");
    printf("....................................................................................\n");
    if(tempnode!=NULL)
    {
        while(tempnode->next!=NULL)
        {
            target_mac[0] = tempnode->mac[0];
            target_mac[1] = tempnode->mac[1];
            target_mac[2] = tempnode->mac[2];
            target_mac[3] = tempnode->mac[3];
            target_mac[4] = tempnode->mac[4];
            target_mac[5] = tempnode->mac[5];
            ip2byte(target_ip, tempnode->ip);
            tempnode->flag = 0;
            memcpy (ether_frame, target_mac, 6 * sizeof (uint8_t));
            strcpy(arphdr.target_ip, target_ip);
            memcpy (ether_frame + ETH_HDRLEN, &arphdr, ARP_HDRLEN * sizeof (uint8_t));
            
            // Send ethernet frame to socket.
            if ((bytes = sendto (sd, ether_frame, frame_length, 0, (struct sockaddr *) &device, sizeof (device))) <= 0) {
                perror ("sendto() failed");
            }
            else{
                //printf("sent to: %s\n", inet_ntoanew(tempnode->ip));
            }
            tempnode = tempnode->next;
            usleep(50000);
        }
            target_mac[0] = tempnode->mac[0];
            target_mac[1] = tempnode->mac[1];
            target_mac[2] = tempnode->mac[2];
            target_mac[3] = tempnode->mac[3];
            target_mac[4] = tempnode->mac[4];
            target_mac[5] = tempnode->mac[5];
            ip2byte(target_ip, tempnode->ip);
            tempnode->flag = 0;
            memcpy (ether_frame, target_mac, 6 * sizeof (uint8_t));
            strcpy(arphdr.target_ip, target_ip);
            memcpy (ether_frame + ETH_HDRLEN, &arphdr, ARP_HDRLEN * sizeof (uint8_t));
            
            // Send ethernet frame to socket.
            if ((bytes = sendto (sd, ether_frame, frame_length, 0, (struct sockaddr *) &device, sizeof (device))) <= 0) {
                perror ("sendto() failed");
            }
            else{
                //printf("sent to: %s\n", inet_ntoanew(tempnode->ip));
            }

    }      
  sleep(2);
   int online=1, offline=0;
   struct ArpTable *temp;
   temp = &arp_table[0];
   temp= temp->next;
   printf("****************************************************************************************\n");
   printf("List of Online Devices:\n");
   printf("****************************************************************************************\n");
   if(temp!=NULL)
   {
        while(temp->next!=NULL)
        {
            printf("....................................................................................\n");
            if(temp->flag==1)
            {
                printf("ONLINE--->   ");
                online++;
                
            }
            else{
                
                printf("OFFLINE--->   ");
                offline++;
                if(temp->status == 1)
                {
                    temp->status = 0;
                    char stringmac[18];
                    mac2string(temp->mac, stringmac);
                    //insertLog(inet_ntoanew(temp->ip),stringmac, "Went Offline", "0.0.0.0", "00:00:00:00:00:00");
                    mongoinserOrUpdateDevice(inet_ntoanew(temp->ip), stringmac, 0, AGENT_ID,hostname);
                    mongoinserOrUpdateStatusChange(stringmac, hostname, "9");
                    pushNotification(stringmac, hostname, "9" );
                }
                
            }
            printf("IP Address: %s          ", inet_ntoanew(temp->ip));
            printf("Mac Address: ");
            displayMac(temp->mac);
            printf("....................................................................................\n");
            temp= temp->next;
            
        }
        printf("....................................................................................\n");
        if(temp->flag==1)
            {
                printf("ONLINE--->   ");
                online++;
            }
            else{
                printf("OFFLINE--->   ");
                if(temp->status == 1)
                {
                    temp->status = 0;
                    char stringmac[18];
                    mac2string(temp->mac, stringmac);
                    //insertLog(inet_ntoanew(temp->ip),stringmac, "Went Offline", "0.0.0.0", "00:00:00:00:00:00");
                    mongoinserOrUpdateDevice(inet_ntoanew(temp->ip), stringmac, 0, AGENT_ID,hostname);
                    mongoinserOrUpdateStatusChange(stringmac, hostname, "9");
                    pushNotification(stringmac, hostname, "9" );
                }
            }
        printf("IP Address: %s          ", inet_ntoanew(temp->ip));
        printf("Mac Address: ");
        displayMac(temp->mac);
        printf("....................................................................................\n");
        
   }
    printf("....................................................................................\n");
    printf("ONLINE--->   ");
    printf("IP Address: %s          ", inet_ntoanew(local.ip));
    printf("Mac Address: ");
    displayMac(local.mac);
    printf("....................................................................................\n");
    printf("Total Online Devices: %d        ",online);
    printf("Total Ofline Devices: %d\n",offline);
    printf("****************************************************************************************\n");
    sleep(60);
}
    close (sd);
   free (ether_frame);
}

int removeNode(uint8_t *mac, int index)
{
    struct ArpTable *node, *previous_node;
    node = &arp_table[0];
    while(node!=NULL)
    {
        if(cmprMac(mac, node->mac)==1 && node->ip!=index2ip(index, local_network.netid))
        {
            previous_node->next = node->next;
            int ip = node->ip;
            node->next = NULL;
            node->ip = 0;
            node->status = 0;
            node->flag =0;
            memset (&node->mac, 0, 6 * sizeof (uint8_t));
            return ip2index(ip, local_network.mask);
        }
        previous_node = node;
        node = node->next;
    }
    return 0;
}

int cmprMac(uint8_t *mac1, uint8_t *mac2)
{
    int i, t=0;
    for(i=0;i<6;i++)
    {
        if(mac1[i]!=mac2[i])
        {
            return 0;
        }

    }
    return 1;
}

void mac2string(uint8_t *mac, char *stringmac)
{
    char strmac[18];
    snprintf(strmac, sizeof(strmac), "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    strcpy(stringmac, strmac);
}

void connect2MongoDatabase()
{
   const char *uri_string = "mongodb://localhost:27017";
   mongoc_uri_t *uri;
   mongoc_collection_t *collection;
   bson_t *insert;
   bson_error_t error;

   mongoc_init ();

   uri = mongoc_uri_new_with_error (uri_string, &error);
   if (!uri) {
      fprintf (stderr,
               "failed to parse URI: %s\n"
               "error message:       %s\n",
               uri_string,
               error.message);
      return;
   }

   client = mongoc_client_new_from_uri (uri);
   if (!client) {
      return;
   }

//    collection = mongoc_client_get_collection (client, "db_name", "coll_name");


//    insert = BCON_NEW ("hello", BCON_UTF8 ("world"));

//    if (!mongoc_collection_insert_one (collection, insert, NULL, NULL, &error)) {
//       fprintf (stderr, "%s\n", error.message);
//    }

//    bson_destroy (insert);
   /*
    * Release our handles and clean up libmongoc
    */
//    mongoc_collection_destroy (collection);
//    mongoc_uri_destroy (uri);
//    mongoc_client_destroy (client);
//    mongoc_cleanup ();
   return;
}

void mongoinserOrUpdateDcoker(char *id, char *mac, char *ip, char *agent_id)
{
    bson_error_t error;
    bson_t *insert;
    mongoc_collection_t *collection;
    bson_t *update = NULL;
    bson_t *query = NULL;
    bson_t *opt = NULL;
     opt = BCON_NEW ("upsert", BCON_BOOL(true));
     query = BCON_NEW ("id", BCON_UTF8 (id));

    update = BCON_NEW ("$set",
                      "{",
                      "mac",
                      BCON_UTF8 (mac),
                      "ip",
                      BCON_UTF8 (ip),
                      "agent_id",
                      BCON_UTF8 (agent_id),
                      "system_type",
                      BCON_UTF8 (SYSTEM_TYPE),
                      "username",
                      BCON_UTF8 (MASTER_VENDOR),
                      "}");

   collection = mongoc_client_get_collection (client, "sclera_nms", "docker");

   if (!mongoc_collection_update_one (collection, query, update, opt, NULL, &error)) {
      fprintf (stderr, "%s\n", error.message);
   }
   bson_destroy(update);
   bson_destroy(query);
   bson_destroy(opt);
   mongoc_collection_destroy (collection);
}

void MongoinsertLog(char *mac, char *ip, char *alarm, char* old_mac, char* old_ip)
{
    bson_error_t error;
    bson_t *insert;
    mongoc_collection_t *collection;
    time_t rawtime;
    struct tm * timeinfo;
    char srttime[11];
    time ( &rawtime );
    ltoa(rawtime, srttime, 10);
    collection = mongoc_client_get_collection (client, "sclera_nms", "log");
    insert = BCON_NEW ("last_seen_on", BCON_UTF8 (srttime), "mac", BCON_UTF8 (mac), "ip", BCON_UTF8 (ip), "alarm", BCON_UTF8 (alarm), "old_mac", BCON_UTF8 (old_mac), "old_ip", BCON_UTF8 (old_ip));
     if (!mongoc_collection_insert_one (collection, insert, NULL, NULL, &error)) {
      fprintf (stderr, "%s\n", error.message);
   }
    bson_destroy (insert);
}

void mongoinserOrUpdateDevice(char* ip, char *mac, int status, char* agent_id, char *docker_id)
{
    bson_error_t error;
    bson_t *insert;
    mongoc_collection_t *collection;
    bson_t *update = NULL;
    bson_t *query = NULL;
    bson_t *opt = NULL;
     opt = BCON_NEW ("upsert", BCON_BOOL(true));
     query = BCON_NEW ("hw_address", BCON_UTF8 (mac), "docker_id", BCON_UTF8 (docker_id));

    update = BCON_NEW ("$set",
                      "{",
                      "hw_address",
                      BCON_UTF8 (mac),
                      "ip_addresses",
                      BCON_UTF8 (ip),
                      "status",
                      BCON_INT32 (status),
                      "agent_id",
                      BCON_UTF8 (agent_id),
                      "docker_id",
                      BCON_UTF8 (docker_id),
                      "}");

   collection = mongoc_client_get_collection (client, "sclera_nms", "devices");
 
   if (!mongoc_collection_update_one (collection, query, update, opt, NULL, &error)) {
      fprintf (stderr, "%s\n", error.message);
   }
   bson_destroy(update);
   bson_destroy(query);
   bson_destroy(opt);
   mongoc_collection_destroy (collection);
}

void mongoinserOrUpdateStatusChange(char *mac, char *docker_id, char *alarm)
{
    bson_error_t error;
    bson_t *insert;
    mongoc_collection_t *collection;
    bson_t *update = NULL;
    bson_t *query = NULL;
    bson_t *opt = NULL;

    time_t rawtime;
    struct tm * timeinfo;
    char srttime[11];
    time ( &rawtime );
    ltoa(rawtime, srttime, 10);

    opt = BCON_NEW ("upsert", BCON_BOOL(true));
    query = BCON_NEW ("hw_address", BCON_UTF8 (mac), "docker_id", BCON_UTF8 (docker_id));

    update = BCON_NEW ("$set",
                      "{",
                      "hw_address",
                      BCON_UTF8 (mac),
                      "docker_id",
                      BCON_UTF8 (docker_id),
                      "last_seen_on",
                      BCON_UTF8 (srttime),
                      "alarm",
                      BCON_UTF8 (alarm),
                      "seen",
                      BCON_UTF8 ("1"),
                      "}");

   collection = mongoc_client_get_collection (client, "sclera_nms", "devices");
 
   if (!mongoc_collection_update_one (collection, query, update, opt, NULL, &error)) {
      fprintf (stderr, "%s\n", error.message);
   }
   bson_destroy(update);
   bson_destroy(query);
   bson_destroy(opt);
   mongoc_collection_destroy (collection);
}

int pushNotification(char *mac, char *docker_id, char *alarm)
{
  CURL *curl;
  CURLcode res;
  char endpoint[120];
  strcpy(endpoint, "http://localhost:2000/notification?");
  strcat(endpoint,"mac");
  strcat(endpoint,"=");
  strcat(endpoint,mac);
  strcat(endpoint, "&");
  strcat(endpoint,"docker_id");
  strcat(endpoint,"=");
  strcat(endpoint,docker_id);
  strcat(endpoint, "&");
  strcat(endpoint,"alarm");
  strcat(endpoint,"=");
  strcat(endpoint,alarm);
  printf("%s\n",endpoint);
 
  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, endpoint);
    /* example.com is redirected, so we tell libcurl to follow redirection */ 
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
 
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
 
    /* always cleanup */ 
    curl_easy_cleanup(curl);
  }
  return 0;
}